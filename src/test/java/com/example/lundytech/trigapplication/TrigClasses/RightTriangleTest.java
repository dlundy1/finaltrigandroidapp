package com.example.lundytech.trigapplication.TrigClasses;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by D.Velop on 10/6/2017.
 */
public class RightTriangleTest {
    RightTriangle triangle = new RightTriangle();
    @Test
    public void findMissingAngle() throws Exception {
        Degree known = new Degree(89);

        assertEquals(1, triangle.FindMissingAngle(known), 0D);

    }

    @Test
    public void findHypotenuse() throws Exception {
        double value = Math.pow(3,2) + Math.pow(2,2);
        double answer = Math.sqrt(value);

        assertEquals(answer, triangle.FindHypotenuse(2,3),0D);
    }

    @Test
    public void findMissingSide() throws Exception {
        double answer = Math.sqrt((Math.pow(3,2)-Math.pow(2,2)));

        assertEquals(answer, triangle.FindMissingSide(3,2),0D);
    }

    @Test
    public void findMissingSide1() throws Exception {
      //  assertEquals(0,triangle.FindMissingSide(15,45),0D);

    }

    @Test
    public void calculateHeight() throws Exception {
        double distance = 15;
        Degree degree = new Degree(55);
        double answer = distance * Math.tan(degree.ToRadian().ToDouble());

        assertEquals(answer,triangle.CalculateHeight(distance,degree),0D);
    }

    @Test
    public void calculateDepth() throws Exception {

    }

}