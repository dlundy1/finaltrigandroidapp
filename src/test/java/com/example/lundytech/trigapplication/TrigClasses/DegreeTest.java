package com.example.lundytech.trigapplication.TrigClasses;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by D.Velop on 10/6/2017.
 */
public class DegreeTest {

    @Test
    public void toDouble() throws Exception {

    }

    @Test
    public void degreeMinutes() throws Exception {
        Degree degree = new Degree(13);
        int answer = (int) (degree.getValue() * 60);
        assertEquals(answer,degree.ToMinutes(),0D);
    }

    @Test
    public void toRadian() throws Exception {
        double answer = 0.0872665;
        Degree degree = new Degree(5);

        assertEquals(answer,degree.ToRadian().ToDouble(),1D);

    }

    @Test
    public void toMinutes() throws Exception {

    }

}